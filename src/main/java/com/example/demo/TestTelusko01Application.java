package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestTelusko01Application {

	public static void main(String[] args) {
		SpringApplication.run(TestTelusko01Application.class, args);
	}

}
